#include "req.h"
#include "session.h"

#define QUEUE_SIZE 128
#define PORT_DEFAULT 8000


int main(int argc, char const *argv[])
{
    cmd_add("load_cloud", "str", "cloud", cmd_load_cloud);
    cmd_add("save_cloud", "cloud, str", "", cmd_save_cloud);
    cmd_add("match_cloud", "cloud, cloud", "cloud, cloud", cmd_match_cloud);
    cmd_add("rmse", "cloud, cloud", "real", cmd_rmse);
    cmd_add("icp", "cloud, cloud, real, int, real", "cloud", cmd_icp);

    unsigned short port = (argc < 2) ? PORT_DEFAULT : atoi(argv[1]);
    timeout = (argc < 3) ? TIMEOUT_DEFAULT : atof(argv[2]);

    struct sockaddr_in channel;
    memset(&channel, 0, sizeof(channel));
    channel.sin_family = AF_INET;
    channel.sin_addr.s_addr = htonl(INADDR_ANY);
    channel.sin_port = htons(port);

    int servsckt = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (servsckt < 0) {
        perror("Error opening socket.\n");
        return -1;
    }

    int on = 1;
    setsockopt(servsckt, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on));

    int b = bind(servsckt, (struct sockaddr *) &channel, sizeof(channel));
    if (b < 0) {
        perror("Error binding.\n");
        return -2;
    }

    int l = listen(servsckt, QUEUE_SIZE);
    if (l < 0) {
        perror("Error listening.\n");
        return -3;
    }

    printf("Listening on port %d.\n", port);

    while(1) {
        // int res = req_handler(servsckt);
        session_timeout();

        int client = accept(servsckt, 0, 0);

        if (client < 0) {
            perror("Error connecting to client.\n");
        } else {
            if (req_handler(client) < 0) {
                perror("Error handling request.\n");
            }

            close(client);
        }
    }

    return 0;
}
