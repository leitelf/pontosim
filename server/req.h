#ifndef REQ_H
#define REQ_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "stdcmd.h"

#define PAYLOAD_SIZE 127
#define PACKET_SIZE 128
#define INT_SIZE 12
#define REAL_SIZE 32

struct buff_lst {
    char *buffer;
    unsigned short size;
    struct buff_lst *next;
};

char *req_receive (int client)
{
    int payload_size = 0;
    struct buff_lst *head = (struct buff_lst*) malloc(sizeof(struct buff_lst));
    struct buff_lst *cur_node = head;

    char header;
    char pckt_payload_size = PAYLOAD_SIZE;
    do {
        int s = read(client, &header, sizeof(char));
        if (s <= 0) {
            return NULL;
        }
        if (header >= 0) {
            pckt_payload_size = header;
        }

        cur_node->buffer = (char *) malloc(pckt_payload_size);

        int pckt_read = 0;
        do {
            s = read(client,
                     &cur_node->buffer[pckt_read],
                     pckt_payload_size - pckt_read);
            if (s <= 0) {
                return NULL;
            }
            pckt_read += s;
        } while(pckt_read < pckt_payload_size);
        payload_size += pckt_payload_size;
        cur_node->size = pckt_payload_size;
        cur_node->next = (struct buff_lst *) malloc(sizeof(struct buff_lst));
        cur_node = cur_node->next;
        cur_node->next = NULL;
    } while (header < 0);

    char *data = (char *) malloc(payload_size + 1);
    int i = 0;
    cur_node = head;

    while (cur_node->next != NULL) {
        memcpy(&data[i], cur_node->buffer, cur_node->size);
        i += cur_node->size;
        free(cur_node->buffer);

        struct buff_lst *tmp = cur_node;
        cur_node = cur_node->next;
        free(tmp);
    }

    free(cur_node);

    data[payload_size] = '\0';

    return data;
}

int req_res(int client, char const *res)
{
    char *pckt = (char *) malloc(PACKET_SIZE);
    int i = 0, bytes_sent = 0;
    int res_len = strlen(res);

    while (res_len > PAYLOAD_SIZE) {
        pckt[0] = (char) -1;
        memcpy(&pckt[1], &res[i], PAYLOAD_SIZE);

        i += PAYLOAD_SIZE;
        res_len -+ PAYLOAD_SIZE;

        int bytes_transmitted = 0;
        do {
            int s = write(client,
                          &pckt[bytes_transmitted],
                          PACKET_SIZE - bytes_transmitted);
            if (s < 0) return -1;
            bytes_transmitted += s;
        } while (bytes_transmitted < PACKET_SIZE);
        bytes_sent += bytes_transmitted;
    }

    pckt[0] = (char) res_len;
    memcpy(&pckt[1], &res[i], res_len);

    int bytes_to_transmit = res_len + 1;
    int bytes_transmitted = 0;
    do {
        int s = write(client, &pckt[bytes_transmitted],
                      bytes_to_transmit - bytes_transmitted);
        if (s < 0) return -1;

        bytes_transmitted += s;
    } while (bytes_transmitted < bytes_to_transmit);

    bytes_sent += bytes_transmitted;

    free(pckt);
    return bytes_sent;
}

int req_handler (int client)
{
    char *buff = req_receive(client);

    printf("\nCMD: %s\n", buff);

    struct cmd *cmd = cmd_get(buff);

    if (cmd == NULL) {
        if(strcmp(buff, SESSION_INIT) == 0) {
            req_res(client, session_init());
        } else if (strcmp(buff, SESSION_END) == 0) {
            char *s_id = req_receive(client);
            session_end(s_id);
            free(s_id);
        } else {
            printf("Invalid command request.\n");
        }
        
    } else {
        char *s_id = req_receive(client);

        printf("session: %s\n", s_id);

        _in_types = cmd->in_lst;
        _in_len = cmd->in_num;
        _in_data = (void **) malloc(sizeof(void *) * _in_len);
        if (_in_len > 0) {
            assert(_in_data != NULL);
        }

        int keep[_in_len];
        char to_del[_in_len][UID_LEN];

        // memset(keep, 0, _in_len);

        for (int i = 0; i < cmd->in_num; i++) {
            char *arg = req_receive(client);
            int arglen = strlen(arg);
            void *aux;

            printf("arg: %s\n", arg);
            switch (cmd->in_lst[i])
            {
            case tInt:
                aux = (int *) malloc(sizeof(int));
                assert(aux != NULL);
                *(int *) aux = atoi(arg);
                break;
            case tReal:
                aux = (double *) malloc(sizeof(double));
                assert(aux != NULL);
                *(double *) aux = atof(arg);
                break;
            case tStr:
                aux = (char *) malloc(sizeof(char) * arglen);
                assert(aux != NULL);
                strcpy((char *) aux, arg);
                break;
            case tCloud:
                if (arg[0] == '!') {
                    keep[i] = 1;
                    aux = session_get_obj(s_id, &(arg[1]))->data;
                } else {
                    keep[i] = 0;
                    strcpy(to_del[i], arg);
                    struct obj *o = session_get_obj(s_id, arg);
                    if (o != NULL) {
                        aux = o->data;
                    } else {
                        aux = NULL;
                    }
                }
                break;
            case tMat:
                if (arg[0] == '!') {
                    keep[i] = 1;
                    aux = session_get_obj(s_id, &(arg[1]))->data;
                } else {
                    keep[i] = 0;
                    strcpy(to_del[i], arg);
                    struct obj *o = session_get_obj(s_id, arg);
                    if (o != NULL) {
                        aux = o->data;
                    } else {
                        aux = NULL;
                    }
                }
                break;
            default:
                break;
            }

            _in_data[i] = aux;

            free(arg);
        }

        _out_types = cmd->out_lst;
        _out_len = cmd->out_num;
        _out_data = (void **) malloc(sizeof(void *) * _out_len);
        if (_out_len > 0) {
            assert(_out_data != NULL);
        }

        for (int i = 0; i < _out_len; i++) {
            void *aux;
            switch (cmd->out_lst[i])
            {
            case tInt:
                aux = (int *) malloc(sizeof(int));
                assert(aux != NULL);
                break;
            case tReal:
                aux = (double *) malloc(sizeof(double));
                assert(aux != NULL);
                break;
            case tStr:
                break;
            default:
                break;
            }
            _out_data[i] = aux;
        }

        cmd->handler();

        for (int i = 0; i < _in_len; i++) {
            if (cmd->in_lst[i] != tCloud && cmd->in_lst[i] != tMat) {
                free(_in_data[i]);
            } else {
                if (keep[i] == 0) {
                    session_delete_obj(s_id, to_del[i]);
                }
                _in_data[i] = NULL;
            }
        }

        if (_in_data != NULL) {
            free(_in_data);
        }
        _in_len = 0;

        char aux_str[sizeof(int)] = "";
        sprintf(aux_str, "%d", _out_len);
        // printf("QTY = %s\n", aux_str);
        req_res(client, aux_str);

        for (int i = 0; i < _out_len; i++) {
            char *res_msg;
            switch (cmd->out_lst[i])
            {
            case tInt:
                res_msg = (char *) malloc(sizeof(char) * INT_SIZE);
                assert(res_msg != NULL);
                sprintf(res_msg, "%d",  * (int *) _out_data[i]);
                break;
            case tReal:
                res_msg = (char *) malloc(sizeof(char) * REAL_SIZE);
                assert(res_msg != NULL);
                sprintf(res_msg, "%lf", * (double *) _out_data[i]);
                break;
            case tStr:
                res_msg = (char *) _out_data[i];
                break;
            case tCloud:
                res_msg = session_save_obj(s_id, _out_data[i], tCloud);
                break;
            case tMat:
                res_msg = session_save_obj(s_id, _out_data[i], tMat);
                break;
            default:
                break;
            }
            req_res(client, res_msg);

            switch (cmd->out_lst[i])
            {
            case tInt:
                free(_out_data[i]);
                break;
            case tReal:
                free(_out_data[i]);
                break;
            default:
                break;
            }

            free(res_msg);
        }

        if (_out_data != NULL) {
            free(_out_data);
        } 
        _out_len = 0;

        _in_types = NULL;
        _out_types = NULL;
        free(s_id);
    }

    free(buff);

    return 0;
}
#endif // REQ_H
