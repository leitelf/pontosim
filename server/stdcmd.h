#ifndef STDCMD_H
#define STDCMD_H

#include "cmd.h"

// int hello ()
// {
//     char *name = (char *) cmd_in(0);

//     char greet[32] = "";

//     sprintf(greet, "Hello, %s!", name);

//     cmd_out_save(0, greet);

//     return 0;
// }

// int sum ()
// {
//     int a = *(int *) cmd_in(0);
//     int b = *(int *) cmd_in(1);

//     int c = a + b;
//     cmd_out_save(0, &c);

//     return 0;
// }

int cmd_load_cloud ()
{
    char *path = (char *) cmd_in(0);
    struct cloud *c = cloud_load_xyz(path);
    cmd_out_save(0, c);
    cloud_free(&c);

    return 0;
}

int cmd_save_cloud ()
{  
    struct cloud *c = (struct cloud *) cmd_in(0);
    if (c != NULL) {
        char *path = (char *) cmd_in(1);
        cloud_save_xyz(c, path);
    }
    
    return 0;
}

// inputs: 2 clouds
// outputs: 2 clouds
int cmd_match_cloud ()
{
    struct cloud *tgt = (struct cloud *) cmd_in(0);
    struct cloud *src = (struct cloud *) cmd_in(1);

    if (cloud_size(src) < cloud_size(tgt)) {
        cmd_out_save(1, src);

        struct cloud *c = registration_closest_points_bf(src, tgt);

        cmd_out_save(0, c);

        cloud_free(&c);
    } else {
        cmd_out_save(0, tgt);

        struct cloud *c = registration_closest_points_bf(tgt, src);

        cmd_out_save(1, c);

        cloud_free(&c);
    }

    return 0;
}

int cmd_rmse ()
{
    struct cloud *tgt = (struct cloud *) cmd_in(0);
    struct cloud *src = (struct cloud *) cmd_in(1);

    real rmse = cloud_rmse(src, tgt);

    cmd_out_save(0, &rmse);

    return 0;
}

int cmd_icp ()
{
    struct cloud *src = (struct cloud *) cmd_in(0);
    struct cloud *tgt = (struct cloud *) cmd_in(1);
    struct cloud *aligned;
    real th = *(real *) cmd_in(2);
    int k = *(int *) cmd_in(3);
    real mdist = *(real *) cmd_in(4);

    registration_icp(src,
                     tgt,
                     &aligned,
                     th,
                     k,
                     mdist,
                     registration_closest_points_bf);
    
    cmd_out_save(0, aligned);

    return 0;
}
// adcionar voxel grid

#endif // STDCMD_H