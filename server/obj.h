#ifndef OBJ_H
#define OBJ_H

#include "pontu/pontu_core.h"
#include "pontu/pontu_features.h"
#include "pontu/pontu_registration.h"
#include "pontu/pontu_sampling.h"
#include <uuid/uuid.h>
#include <assert.h>

#define UID_LEN 37

typedef enum {oCloud, oMat} obj_type;

struct obj {
    obj_type type;
    char oid[UID_LEN];
    void *data;
};

struct obj_node {
    struct obj *obj;
    struct obj_node *next;
};

struct obj *obj_get (char const *oid, struct obj_node *objs)
{
    for (struct obj_node *aux = objs; aux != NULL; aux = aux->next) {
        if (strcmp(oid, aux->obj->oid) == 0) {
            return aux->obj;
        }
    }

    return NULL;
}

int obj_delete (char const *oid, struct obj_node **objs, int objs_len)
{
    struct obj_node* cur = *objs;
    struct obj_node* prev = NULL;
    if (cur == NULL) {
        return 0;
    }

    while (strcmp(oid, cur->obj->oid) != 0) {
        if (cur->next == NULL) {
            return objs_len;
        } else {
            prev = cur;
            cur = cur->next;
        }
    }

    if (cur == *objs) {
        *objs = (*objs)->next;
    } else {
        prev->next = cur->next;
    }

    switch (cur->obj->type)
    {
    case oCloud:
        cloud_free((struct cloud **) &(cur->obj->data));
        break;
    case oMat:
        matrix_free((struct matrix **) &(cur->obj->data));
        break;
    default:
        free(cur->obj->data);
        break;
    }

    free(cur->obj);
    cur->obj = NULL;
    free(cur);

    return --objs_len;
}

int obj_update (char const *oid,
                void *val,
                obj_type type,
                struct obj_node **objs,
                int objs_len)
{
    if (strlen(oid) >= UID_LEN) {
        perror("Invalid object id.\n");
        return objs_len;
    }

    struct obj* o = obj_get(oid, *objs);

    if (o == NULL) {
        o = (struct obj *) malloc(sizeof(struct obj));
        assert (o != NULL);
        o->type = type;
        strcpy(o->oid, oid);
        switch (type)
        {
        case oCloud:
            o->data = cloud_copy((struct cloud *) val);
            break;
        case oMat:
            o->data = matrix_copy((struct matrix *) val);
            break;
        default:
            break;
        }

        struct obj_node *node = (struct obj_node *) 
                                malloc(sizeof(struct obj_node));
        assert(node != NULL);

        node->obj = o;
        node->next = *objs;

        *objs = node;
        return ++objs_len;
    } else {
        char aux[UID_LEN] = "";
        strcpy(aux, oid);
        objs_len = obj_delete(oid, objs, objs_len);
        return obj_update(aux, val, type, objs, objs_len);
    }
}

int obj_save (void *val,
              obj_type type,
              struct obj_node **objs,
              int objs_len,
              char **out_oid)
{
    uuid_t oid;
    *out_oid = (char *) malloc(UID_LEN);

    assert(*out_oid != NULL);

    uuid_generate_time_safe(oid);
    uuid_unparse_lower(oid, *out_oid);

    return obj_update(*out_oid, val, type, objs, objs_len);
}

void obj_lst_print (struct obj_node *objs, int objs_len)
{
    if ((objs_len == 0) || (objs == NULL)) {
        printf("Empty object list.\n");
    }

    int i = 0;
    for (struct obj_node *aux = objs; aux != NULL; aux = aux->next) {
        printf("[%d]:\n\toid = %s;\n\ttype = %d.\n",
               ++i,
               aux->obj->oid,
               aux->obj->type);
    }
}

int obj_lst_clear (struct obj_node **objs, int objs_len)
{
    if (objs_len > 0) {
        assert (*objs != NULL);
        while (objs_len > 0) {
            objs_len = obj_delete((*objs)->obj->oid, objs, objs_len);
        }
        *objs = NULL;
    } else {
        *objs = NULL;
    }

    return 0;
}

#endif // OBJ_H