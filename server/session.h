#ifndef SESSION_H
#define SESSION_H

// #include <time.h>
// #include <stdio.h>
#include "obj.h"

#define TIMEOUT_DEFAULT 60


struct session {
    char uid[UID_LEN];
    time_t created;
    struct obj_node *objs;
    int objs_len;
};

struct session_node {
    struct session *session;
    struct session_node *next;
};

struct session_node *sessions = NULL;
int sessions_len = 0;

double timeout = TIMEOUT_DEFAULT;

char const *session_init ()
{
    uuid_t uid;
    char straux[UID_LEN];
    uuid_generate_time_safe(uid);
    uuid_unparse_lower(uid, straux);

    struct session* s = (struct session *) malloc(sizeof(struct session));
    assert (s != NULL);
    s->objs_len = 0;
    s->objs = NULL;
    strcpy(s->uid, straux);
    time(&(s->created));

    struct session_node *node = (struct session_node *)
                                malloc(sizeof(struct session_node));
    assert (node != NULL);

    node->session = s;
    node->next = sessions;

    sessions = node;
    sessions_len++;

    return s->uid;
}

void session_end (char const *uid)
{
    struct session_node *cur = sessions;
    struct session_node *prev = NULL;

    if (cur == NULL) {
        return;
    }

    while (strcmp(uid, cur->session->uid) != 0) {
        if (cur->next == NULL) {
            return;
        } else {
            prev = cur;
            cur = cur->next;
        }
    }

    if (cur == sessions) {
        sessions = sessions->next;
    } else {
        prev->next = cur->next;
    }

    obj_lst_clear(&(cur->session->objs), cur->session->objs_len);

    free(cur->session);
    cur->session = NULL;
    free(cur);

    sessions_len--;
}

void session_endall ()
{
    if (sessions_len > 0) {
        assert (sessions != NULL);
        while (sessions_len > 0) {
            session_end(sessions->session->uid);
        }

        sessions = NULL;
        sessions_len = 0;
    } else {
        sessions = NULL;
    }
}

void session_timeout ()
{
    time_t cur_time;
    time(&cur_time);
    double diff_time;

    struct session_node *aux = sessions;
    struct session_node *next = NULL;
    while (aux != NULL) {
        next = aux->next;
        diff_time = difftime(cur_time, aux->session->created);
        if (diff_time >= 60 * timeout) {
            session_end(aux->session->uid);
        }
        aux = next;
    }
}

struct session *session_get (char const *uid)
{
    for (struct session_node *aux = sessions; aux != NULL; aux = aux->next) {
        if (strcmp(uid, aux->session->uid) == 0) {
            return aux->session;
        }
    }

    return NULL;
}

struct obj *session_get_obj (char const *uid, char const *oid)
{
    struct session *s = session_get(uid);

    if (s == NULL) {
        return NULL;
    }

    return obj_get(oid, s->objs);
}

char *session_save_obj (char const *uid, void *val, obj_type type)
{
    struct session *s = session_get(uid);

    if (s == NULL) {
        return NULL;
    }

    char *oid;
    s->objs_len = obj_save(val, type, &(s->objs), s->objs_len, &oid);

    return oid;
}

void session_delete_obj (char const *uid, char const *oid)
{
    struct session *s = session_get(uid);
    if (s == NULL) {
        return;
    }

    s->objs_len = obj_delete(oid, &(s->objs), s->objs_len);
}

void session_update_obj (char const *uid,
                         char const *oid,
                         void *val,
                         obj_type type) 
{
    struct session *s = session_get(uid);
    if (s == NULL) {
        return;
    }

    s->objs_len = (oid, val, type, &(s->objs), s->objs_len);
}


#endif // SESSION_H