#ifndef CMD_H
#define CMD_H

// #include <stdlib.h>
// #include <stdio.h>
// #include <assert.h>
// #include <string.h>
#include "session.h"

#define MAX_NAME_SIZE 64
#define TYPE_SIZE 6

#define SESSION_INIT "session_init"
#define SESSION_END "session_end"

typedef enum {tCloud, tMat, tInt, tReal, tStr, tInvalid} type;

typedef int (*cmd_cb) ();

struct cmd {
    char *name;
    
    int in_num;
    type *in_lst;
    
    int out_num;
    type *out_lst;

    cmd_cb handler;
};

struct cmd **cmd_lst = NULL;
unsigned short cmds_len = 0;

void **_in_data;
int _in_len;
type *_in_types;

void **_out_data;
int _out_len;
type *_out_types;

// 0 - OK
// -1 - Invalid command name
// -2 - Invalid input types
// -3 - Invalid output types
struct cmd *cmd_get(char const *name)
{
    for (int i = 0; i < cmds_len; i++) {
        if (strcmp(cmd_lst[i]->name, name) == 0) {
            return cmd_lst[i];
        }
    }
    return NULL;
}

int cmd_types (char const *types_str, type **types_lst)
{
    int i = 0;
    int j = 0;
    int lst_size = 0;
    
    char trimmed_str[strlen(types_str) + 1];

    while(types_str[i] != '\0' && i < strlen(types_str)) {
        if (types_str[i] != ' ') {
            trimmed_str[j++] = types_str[i];
        }
        i++;
    }

    trimmed_str[j] = '\0';
    i = 0;
    j = 0;

    while (trimmed_str[i] != '\0' && i < strlen(trimmed_str)) {
        char aux[TYPE_SIZE];
        type aux_type;
        j = 0;
        while (trimmed_str[i] != ',' &&
               j < TYPE_SIZE - 1 &&
               trimmed_str[i] != '\0' &&
               i < strlen(trimmed_str))
        {
            aux[j++] = trimmed_str[i++];
        }

        aux[j] = '\0';
        
        if (trimmed_str[i] != ',' && trimmed_str[i] != '\0') {
            aux_type = tInvalid;
        } else if (strcmp(aux, "cloud") == 0) {
            aux_type = tCloud;
        } else if (strcmp(aux, "mat") == 0) {
            aux_type = tMat;
        } else if (strcmp(aux, "int") == 0) {
            aux_type = tInt;
        } else if (strcmp(aux, "real") == 0) {
            aux_type = tReal;
        } else if (strcmp(aux, "str") == 0) {
            aux_type = tStr;
        } else {
            aux_type = tInvalid;
        }

        if (aux_type == tInvalid) {
            printf("Invalid type list.\n");
            if (lst_size > 0) {
                free(*types_lst);
            }
            return -1;
        }

        if (lst_size++ == 0) {
            *types_lst = (type *) malloc(sizeof(type));
        } else {
            *types_lst = (type *) realloc(*types_lst, sizeof(type) * lst_size);
        }

        assert(*types_lst != NULL);
        (*types_lst)[lst_size - 1] = aux_type;

        i++;
    }
    return lst_size;
}

int cmd_add (char const *name, char const *in, char const *out, cmd_cb cb)
{
    if (cmd_get(name) != NULL) {
        printf("Command \"%s\" already exist.\n", name);
        return -1;
    }

    if (strcmp(name, SESSION_INIT) == 0 || strcmp(name, SESSION_END) == 0) {
        printf("Command name \"%s\" invalid.\n", name);
        return -1;
    }

    type *inputs;
    int input_len = cmd_types(in, &inputs);
    if (input_len < 0) return -2;

    type *outputs;
    int output_len = cmd_types(out, &outputs);
    if (input_len < 0) return -3;

    struct cmd *cur_cmd;
    cur_cmd = (struct cmd *) malloc(sizeof(struct cmd));
    assert (cur_cmd != NULL);

    cur_cmd->name = (char *) malloc(sizeof(char) * (strlen(name) + 1));
    assert(cur_cmd->name != NULL);
    strcpy(cur_cmd->name, name);
    cur_cmd->in_num = input_len;
    cur_cmd->in_lst = inputs;
    cur_cmd->out_num = output_len;
    cur_cmd->out_lst = outputs;
    cur_cmd->handler = cb;

    if (cmds_len == 0) {
        cmd_lst = (struct cmd **) malloc(sizeof(struct cmd *));
        cmds_len++;
    } else {
        cmd_lst = (struct cmd **) realloc(cmd_lst,
                                          ++cmds_len * sizeof(struct cmd *));
    }

    assert(cmd_lst != NULL);

    cmd_lst[cmds_len - 1] = cur_cmd;
}

void cmd_exec (char const *name)
{
    struct cmd *cmd = cmd_get(name);

    if (cmd == NULL) {
        printf("Invalid command.\n");
    }

    cmd->handler();
}

type cmd_in_type (int i)
{
    if (i < 0 || i >= _in_len) return -1;
    return _in_types[i];
}

type cmd_out_type (int i)
{
    if (i < 0 || i >= _out_len) return -1;
    return _out_types[i];
}

void *cmd_in (int i)
{
    if (i < 0 || i >= _in_len) return NULL;
    
    return _in_data[i];
}

void cmd_out_save (int i, void *data)
{
    if (i < 0 || i >= _out_len) return;

    switch (cmd_out_type(i))
    {
    case tInt:
        * (int *) _out_data[i] = * (int *) data;
        break;
    case tReal:
        * (double *) _out_data[i] = * (double *) data;
        break;
    case tStr:
        _out_data[i] = (char *) malloc(strlen((char *) data));
        if (strlen((char *) data) > 0) assert(_out_data[i] != NULL);
        strcpy(_out_data[i], data);
        break;
    case tCloud:
        _out_data[i] = cloud_copy(data);
        assert(_out_data[i] != NULL);
        break;
    case tMat:
        _out_data[i] = matrix_copy(data);
        assert(_out_data[i] != NULL);
        break;
    default:
        break;
    }
}

#endif // CMD_H
