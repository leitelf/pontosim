# pontoSim

<h2>Sobre</h2>
<hr>
<p style="text-align: justify;">pontoSim é um arcabouço para concepção e simulação de <i>pipelines</i> de processamento de nuvens de pontos 3D, que utiliza estruturas de Redes de Petri Coloridas (RPC) e baseado na biblioteca <a href="https://gitlab.com/interfacesufc/pontu">Pontu</a>.</p>
<h2>Arquitetura</h2>
<hr>
<p style="text-align: justify;">O arcabouço segue uma arquitetura cliente-servidor, em que o componente servidor é responsável pelo processamento dos dados e o componente cliente pela interface com o usuário.</p>
<h2>Comunicação</h2>
<hr>
<p style="text-align: justify;">Existem três tipos de interações entre cliente e servidor: (<b>i</b>) iniciar uma sessão; (<b>ii</b>) requisitar um comando; e (<b>iii</b>) encerrar sessão. Essas interações seguem os protocolos ilustrado à seguir para a troca de mensagens.</p>

<br/>

<div style="display: block; margin-left: auto; margin-right: auto; width: 50%; padding: 6px; border: 1px solid black; width:fit-content">
    <img src="assets/imgs/init_session.png" alt="Protocolo de inicialização de sessão" />
    <br/>
    <p style="text-align: center; margin-top: 6px;"><b>Fig. 1</b>: Protocolo de inicialização de sessão.</p>
</div>
<br/>
<div style="display: block; margin-left: auto; margin-right: auto; width: 50%; padding: 6px; border: 1px solid black; width:fit-content">
    <img src="assets/imgs/req_cmd.png" alt="Protocolo de requisição de comando" />
    <br/>
    <p style="text-align: center; margin-top: 6px;"><b>Fig. 2</b>: Protocolo de requisição de comando.</p>
</div>
<br/>

<div style="display: block; margin-left: auto; margin-right: auto; width: 50%; padding: 6px; border: 1px solid black; width:fit-content">
    <img src="assets/imgs/end_session.png" alt="Protocolo de finalização de sessão" />
    <br/>
    <p style="text-align: center; margin-top: 6px;"><b>Fig. 3</b>: Protocolo de finalização de sessão.</p>
</div>

<h2>Tipos de dados</h2>
<hr/>
<p style="text-align: justify">São tratados cinco tipos de dados: números inteiros, números reais, textos, nuvens de pontos 3D (NP3D) e matrizes. Os dois últimos são tratados como objetos.</p>
<p style="text-align: justify">Toda mensagem trocada entre cliente e servidor deve ser enviada em formato de texto. O servidor faz a conversão do formato texto para formatos de interesse de maneira automática. O cliente necessita que seja feita a conversão de maneira explícita.</p>

<p style="text-align: justify">Dados dos tipos NP3D e matrizes não são transferidos para o cliente. Esses dados são mantidos na memória do servidor e são enviados apenas identificadores desses dados no servidor. Esses dados, junto aos seus identificadores únicos, são chamados de objetos no arcabouço.</p>

<p style="text-align: justify">Após a execução de um comando, o comportamento padrão do servidor é de excluir os objetos que foram utilizados como parâmetros de entrada para o comando. Os objetos podem ser mantidos se utilizada a função para definir tal comportamento.</p>


<h2>Sessões</h2>
<hr>
<p style="text-align: justify">As sessões identificam a que cliente pertence uma lista de objetos. sessões podem ser iniciadas pelo cliente com a função <code>init_session</code> e encerradas em dois casos: (<b>i</b>) finalizada pelo cliente com função <code>end_session</code>; ou (<b>ii</b>) ao atingir um tempo limite.</p>
<p style="text-align: justify">O tempo limite de uma sessão impede que uma simulação aloque por tempo indeterminado o servidor, caso a simulação falhe, não seja concluída ou o usuário não defina o fim da sessão.</p>

<h2>Funções</h2>
<hr/>
<p style="text-align: justify">A tabela a seguir lista as funções atualmente implementados no arcabouço que podem ser utilizadas no cliente para interação com servidor.</p>

| Função  | Entradas  | Saidas  | Descrição  |
| ------------ | ------------ | ------------ | ------------ |
| `init_session`  |  |  | Inicia sessão.  |
| `end_session`  |  |   | Encerra sessão.  |
| `request`  | nome do comando (texto), lista de parâmetros de entrada para comando ([texto]) | lista de saidas do comando requisitado ([texto])  | Requisita execução de comando ao servidor. 
| `keep`  | identificador de objeto do tipo NP3D ou matriz (texto)  | identificador com *flag* para não deleção após uso em servidor  | Adiciona *flag* para que objeto seja mantido após uso em servidor.|


<h2>Comandos</h2>
<hr/>
<p style="text-align: justify">A tabela a seguir lista os comandos atualmente implementados no arcabouço que podem ser requisitados pelo cliente ao servidor.</p>


| Comando  | Entradas  | Saidas  | Descrição  |
| ------------ | ------------ | ------------ | ------------ |
| `load_cloud`  | arquivo (texto)  | nuvem (NP3D)  | Carrega NP3D de arquivo formato "xyz" na memória.  |
| `save_cloud`  | nuvem (NP3D), arquivo (texto) |   | Salva NP3D de memória em arquivo no formato "xyz".  |
| `icp`  | nuvem de entrada (NP3D), nuvem alvo (NP3D), tolerância de erro (real), máximo iterações (inteiro), tolerância de distância (real)  | nuvem corrigida (NP3D)  | Alinha nuvem de entrada a nuvem alvo com algoritmo ICP.  |
| `match_cloud`  | nuvem de alvo (NP3D), nuvem de entrada (NP3D)  | nuvem com pontos correspondentes de nuvem alvo (NP3D), nuvem como pontos correspondentes de nuvem de entrada (NP3D)  | Econtra pontos correspondentes entre nuvens de entrada e nuvem alvo, utilizando mínima distância Euclidiana como critério de correspondência. Tamanho de nuvens resultantes definido pelo tamanho da menor nuvem (entrada ou alvo).|
| `rmse`  | nuvem alvo (NP3D), nuvem de entrada (NP3D)  | RMSE (real)  | Calcula RMSE entre duas nuvens de mesmo tamanho. |

<h2>Exemplo</h2>
<hr>
<p style="text-align: justify;">Um vídeo demonstrativo da pontoSim está disponível <a href="https://gitlab.com/leitelf/pontosim/-/blob/master/assets/vid/pontosimpres.mp4?expanded=true&viewer=rich" target="_blank">aqui</a>. O vídeo demonstra o uso da pontoSim para aplicação de registro de nuvens de pontos com algoritmo ICP implementado para Pontu. Além da demonstração o vídeo traz comparações com o uso direto das bibliotecas Pontu e PCL.</p>

<h2>Adição de comandos</h2>
<hr>
<p style="text-align: justify;">Para adicionar novos comandos ao arcabouço é necessário:
    <ol>
        <li> implementar função que será chamada quando comando for requisitado; e</li>
        <li> adicionar comando e sua assinatura à lista de comandos.</li>
    </ol></p>

<p style="text-align: justify;">A função deve ser adicionada no arquivo <code>stdcmd.h</code> do servidor. Para acessar os dados de entrada enviados pelo cliente, deve-se usar a função <code>cmd_in</code>, que recebe como parâmetro o índice (iniciado em zero) do parâmetro de entrada. Para definir uma saída do comando deve-se utilizar o comando <code>cmd_out_save</code>, que recebe como parâmetros o índice (iniciado em zero) da saída e o valor que será definido como saída. A função não deve ter parâmetros de entrada e deve retornar um inteiro.</p>
<br>

```
int hello ()
{
    char *name = (char *) cmd_in(0);
    char greet[32] = "";

    sprintf(greet, "Hello, %s!", name);

    cmd_out_save(0, greet);

    return 0;
}
```

<p style="text-align: justify;">Com a função implementada, o comando pode ser registrado no arcabouço, definindo na função <code>main</code> do arquivo <code>server.c</code> como o exemplo a seguir:</p>

```
...
    cmd_add("hello", "str", "str", cmd_hello);
...
```

<p style="text-align: justify;">A função <code>cmd_add</code> espera como parâmetros, nessa ordem, o nome que será utilizado para chamar o comando, os tipos (int, real, str, cloud, mat) dos parâmetros de entrada separados por vírgula, os tipos dos parâmetros de saída separados por vírgula e a função para executar ao chamar o comando.</p>

<p style="text-align: justify;">Para utilizar bibliotecas além da Pontu, basta incluir as referências aos cabeçalhos da biblioteca que se deseja no arquivo <code>stdcmd.h</code> e linkar os objetos necessários durante a compilação.<p>